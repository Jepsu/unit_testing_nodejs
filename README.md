# Unit_Testing by Evgeni Egorov

### This projects contains main.js file with endpoints for each operation, mylib.js file with operations and also mylib.test.js files to test units of main and mylib.js files.

## How to start
### 1. Create new directory on your pc and git init in it or clone existing repository from your githost service.
### 2. Install dependencies (listed in package.json file)
### 3. Write code and start the app.

## Terminal Commands

Code Block
 
```sh 
git clone
https://gitlab.com/Jepsu/unit_testing_nodejs.git

cd ./unit_testing_nodejs

npm install 

```
## Well Done !

![alt text](https://blog.ippon.tech/content/images/2021/09/Intro-to-Unit-Testing-Where-Do-I-Start-Mocha-Or-Jest.png)



