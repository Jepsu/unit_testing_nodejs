#/bin/bash

git init 
npm init -y
npm install express --save
npm install --save-dev mocha chai

mkdir src
mkdir test

touch src/main.js
touch src/mylib.js
touch test/mylib.test.js
touch README.md
touch  .gitignore
