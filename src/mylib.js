const express = require('express')
const bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.json());

/**
 * This arrow function  return sum of two parameters
 * @param {number} param1 
 * @param {number} param2 
 * @returns {number}
 */
const addition =(param1, param2) => {
    const result = param1 + param2;
    return result;
}

/**
 * Arrow function without curly brackets it does not require return keyword !!! Subtraction of two parameters.
 * @param {number} a 
 * @param {number} b 
 * @returns 
 */
const subtraction = (a,b) => a-b;


/**
 * This arrow function  return result of multiplication of two parameters.
 * @param {number} param1 
 * @param {number} param2 
 * @returns {number}
 */
const multiplication =(param1, param2) => {
    const result = param1 * param2;
    return result;
}

/**
 * This arrow function  return result of division  of two parameters.
 * @param {number} param1 
 * @param {number} param2 
 * @returns {number}
 */
const division = (param1, param2, res) => {
    if (param2 === 0) {
        return res.status(400).json({ error: "Division by zero is not allowed." });
    } else {
        const result = param1 / param2;
        return res.json({ result });
    }
}



module.exports = {
    addition:addition,
    subtraction:subtraction,
    multiplication: multiplication,
    division:division

    
}
