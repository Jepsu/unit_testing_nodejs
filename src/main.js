const express = require('express')
const bodyParser = require('body-parser');
const mylib = require("./mylib")

const app = express()
const port = 3000


app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Main endpoint')
});

app.get('/addition', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const sum = mylib.addition(a,b)
    res.send(sum.toString());
})
app.get('/subtraction', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const sub = mylib.subtraction(a,b)
  res.send(sub.toString());
})
app.get('/multiplication', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const mult = mylib.multiplication(a,b)
  res.json(mult);
})
app.get('/division', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);

  if (isNaN(a) || isNaN(b)) {
      return res.status(400).json({ error: "Invalid input. Both parameters must be numbers." });
  }

  mylib.division(a, b, res);
})

app.listen(port, () => {
  console.log(`Server is running on: http://localhost:${port}`)
});

module.exports = app.listen()

//Example to test api
//http://localhost:3000/division?a=1&b=0
