const chai = require('chai');
const chaiHttp = require('chai-http');
const assert = chai.assert;
const app = require('../src/main'); 
chai.should();
chai.use(chaiHttp);

// Testing
describe("Testing first endpoint of express app main.js", () => {
    describe("GET /", () => {
        it("Should get response status 200 from first endpoint", (done) => {
             chai.request(app)
                 .get('/')
                 .end((err, res) => {
                     res.should.have.status(200);
                     done();
                  });
         });   
    });
});

describe("Testing devision endpoint of express app main.js", () => {
    describe("/division", () => {
        it("Should get response status 200 from division endpoint", (done) => {
             chai.request(app)
                 .get('/division?a=10&b=2')
                 .end((err, res) => {
                     res.should.have.status(200);
                     done();
                  });
         });   
    });
});

// Test