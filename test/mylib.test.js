const chai = require('chai');
const assert = chai.assert;
const chaiHttp = require('chai-http');
const expect = require("chai").expect;
const app = require('../src/main.js');
const mylib = require("../src/mylib.js");

chai.use(chaiHttp);

describe("Testing mylib.js file`s addition() function", () => {
    it("The Function addition() should return 2 when function`s parameters are a=1, b=1", () => {
        const result = mylib.addition(1, 1);
        expect(result).to.equal(2);
    })
    before(() => console.log("Starting addition() function tests!"));
    after(() => console.log("All tests are done !"));
 });

describe("Testing mylib.js file`s subtraction() function", () => {
    it("The Function addition() should return 0 when function`s parameters are a=1, b=1", () => {
        const result = mylib.subtraction(1, 1);
        expect(result).to.equal(0);
    })
    before(() => console.log("Starting addition function() tests!"));
    after(() => console.log("All tests are done !"));
 });
 
describe("Testing mylib.js file`s multiplication() function", () => {
    it("The Function multiplication() should return 4 when function`s parameters are a=2, b=2", () => {
        const result = mylib.multiplication(2, 2);
        expect(result).to.equal(4);
    })
    before(() => console.log("Starting multiplication() function tests!"));
    after(() => console.log("All tests are done !"));
 });


describe('Testing mylib.js file`s division() function', () => {
    describe("division /", () => {
        it('should throw an error when dividing by zero', (done) => {
            chai.request(app)
                .get('/division?a=10&b=0') 
                .end((err, res) => {
                    assert.equal(res.status, 400);
                    assert.deepEqual(res.body, { error: "Division by zero is not allowed." });
                    done();
        it('should handle division', (done) => {
            chai.request(app)
                .get('/division?a=10&b=2') 
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.deepEqual(res.body, { result: 5 });
                    done();
          });
        });
    });
  });
});
});



